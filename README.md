### <div align="center">I'm Rick, a DevOps Engineer 💻 and CS & Engineering Student 🎓 </div>  
  

- 🔭 I’m currently working on my [Kubernetes based Homelab](https://gitlab.com/nuzzzen/homelab-kubernetes) and learning Go.
    

- ❓ Ask me about anything related to Linux, Kubernetes, Docker and related technologies.


- 🏋️ I practice Olympic Weightlifting and sometimes participate in competitions.


- ⚡ Fun fact: I prefer GitLab to Github. Oh, and btw I use Arch.  
  

- 📄 My CV is available [in this repo](https://gitlab.com/nuzzzen/nuzzzen-cv) or [here in PDF version](https://nuzzzen.gitlab.io/nuzzzen-cv/cv.pdf) (it needs some updates).  
  

<br/>  


## My Skill Set  
<table><tr><td valign="top" width="33%">



### Frontend  
<div align="center">  
<a href="https://www.javascript.com/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/javascript-original.svg" alt="JavaScript" height="50" /></a>
</td><td valign="top" width="33%">


### Backend  
<div align="center">  

<a href="https://go.dev/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/go-original.svg" alt="Go" height="50" /></a>
<a href="https://www.java.com/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/java-original-wordmark.svg" alt="Java" height="50" /></a>
<a href="https://docs.spring.io/spring-framework/docs/3.0.x/reference/expressions.html#:~:text=The%20Spring%20Expression%20Language%20(SpEL,and%20basic%20string%20templating%20functionality." target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/springio-icon.svg" alt="Spring" height="50" /></a>
<a href="https://quarkus.io/" target="_blank"><img style="margin: 10px" src="https://seeklogo.com/images/Q/quarkus-logo-C9F006782E-seeklogo.com.png" alt="Quarkus" height="50" /></a> 
<a href="https://www.mysql.com/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/mysql-original-wordmark.svg" alt="MySQL" height="50" /></a>
<a href="https://www.postgresql.org/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/postgresql-original-wordmark.svg" alt="PostgreSQL" height="50" /></a>  
<a href="https://www.nginx.com/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/nginx-original.svg" alt="Nginx" height="50" /></a>  

</td><td valign="top" width="33%">



### DevOps  
<div align="center">  
<a href="https://aws.amazon.com/" target="_blank"><img style="margin: 10px" src="https://brandlogos.net/wp-content/uploads/2021/11/amazon_web_services-logo.png" alt="AWS" height="50" /></a>   
<a href="https://kubernetes.io/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/kubernetes-icon.svg" alt="Kubernetes" height="50" /></a>  
<a href="https://www.linux.org/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/linux-original.svg" alt="Linux" height="50" /></a>  
<a href="https://github.com/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/git-scm-icon.svg" alt="Git" height="50" /></a>  
<a href="https://www.gnu.org/software/bash/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/gnu_bash-icon.svg" alt="Bash" height="50" /></a>  
<a href="https://www.terraform.io/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/terraformio-icon.svg" alt="Terraform" height="50" /></a>
<a href="https://www.ansible.com/" target="_blank"><img style="margin: 10px" src="https://creazilla-store.fra1.digitaloceanspaces.com/icons/3269501/ansible-icon-md.png" alt="Ansible" height="50" /></a>  
<a href="https://about.gitlab.com/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/gitlab.svg" alt="GitLab" height="50" /></a>  
<a href="https://www.docker.com/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/docker-original-wordmark.svg" alt="Docker" height="50" /></a>  
<a href="https://argoproj.github.io/cd/" target="_blank"><img style="margin: 10px" src="https://icons-for-free.com/iff/png/512/argocd-1331550886883580947.png" alt="ArgoCD" height="50" /></a> 
<a href="https://azure.microsoft.com/en-in/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/microsoft_azure-icon.svg" alt="Azure" height="50" /></a>  
<a href="https://cloud.google.com/" target="_blank"><img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/google_cloud-icon.svg" alt="GCP" height="50" /></a> 
</div>


</td></tr></table>  

<br/>  


## Connect with me  
<div align="center">
<a href="https://github.com/nuzzzen" target="_blank">
<img src=https://img.shields.io/badge/github-%2324292e.svg?&style=for-the-badge&logo=github&logoColor=white alt=github style="margin-bottom: 5px;" />
</a>
<a href="https://linkedin.com/in/ricknz" target="_blank">
<img src=https://img.shields.io/badge/linkedin-%231E77B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white alt=linkedin style="margin-bottom: 5px;" />
</a>
<a href="https://gitlab.com/nuzzzen" target="_blank">
<img src=https://img.shields.io/badge/gitlab-fc6d27.svg?&style=for-the-badge&logo=gitlab&logoColor=white alt=gitlab style="margin-bottom: 5px;" />
</a>
<a href="mailto:info@ric.nz" target="_blank">
<img src=https://img.shields.io/badge/info@ric.nz-grey?style=for-the-badge&logo=apple&logoColor=white style="margin-bottom: 5px;" />
</a>  
</div>  

<br/>  


## GitLab Stats  
<div align="center"><img src="https://gitlab-readme-stats-beryl.vercel.app/api?username=nuzzzen" align="center" /></div>  

<br/>  

<br/>  

<div align="center">
            <a href="https://paypal.me/ricknz" target="_blank" style="display: inline-block;">
                <img
                    src="https://img.shields.io/badge/PayPal-00457C?style=for-the-badge&logo=paypal&logoColor=white" 
                    align="center"
                />
            </a></div>
<br />

----
<div align="center">Generated using <a href="https://profilinator.rishav.dev/" target="_blank">Github Profilinator</a></div>
